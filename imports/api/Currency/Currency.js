/* eslint-disable consistent-return */

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Currency = new Mongo.Collection('Currency');

Currency.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Currency.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Currency.schema = new SimpleSchema({
  owner: {
    type: String,
    label: 'The ID of the user this currency belongs to.',
  },
  createdAt: {
    type: String,
    label: 'The date this currency was created.',
    autoValue() {
      if (this.isInsert) return (new Date()).toISOString();
    },
  },
  updatedAt: {
    type: String,
    label: 'The date this currency was last updated.',
    autoValue() {
      if (this.isInsert || this.isUpdate) return (new Date()).toISOString();
    },
  },
  name: {
    type: String,
    label: 'The name of the currency.',
  },
  country: {
    type: String,
    label: 'The country of the currency.',
  },
  symbol: {
    type: String,
    label: 'The symbol of the currency.',
  },
  image: {
    type: String,
    label: 'The symbol of the currency.',
  },
});

Currency.attachSchema(Currency.schema);

export default Currency;
