import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Currency from '../Currency';

Meteor.publish('currency', function currency() {
  return Currency.find();
});

// Note: currency.view is also used when editing an existing currency.
Meteor.publish('currency.view', function currencyView(currencyId) {
  check(currencyId, String);
  return Currency.find();
});
