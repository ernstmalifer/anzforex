import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Currency from './Currency';
import rateLimit from '../../modules/rate-limit';

Meteor.methods({
  'currency.insert': function currencyInsert(curr) {
    check(curr, {
      name: String,
      country: String,
      symbol: String,
      image: String,
    });

    try {
      return Currency.insert({ owner: this.userId, ...curr });
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'currency.update': function currencyUpdate(curr) {
    check(curr, {
      _id: String,
      name: String,
      country: String,
      symbol: String,
      image: String,
    });

    try {
      const currencyId = curr._id;
      Currency.update(currencyId, { $set: curr });
      return currencyId; // Return _id so we can redirect to currency after update.
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'currency.remove': function currencyRemove(currencyId) {
    check(currencyId, String);

    try {
      return Currency.remove(currencyId);
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
});

rateLimit({
  methods: [
    'currency.insert',
    'currency.update',
    'currency.remove',
  ],
  limit: 5,
  timeRange: 1000,
});
