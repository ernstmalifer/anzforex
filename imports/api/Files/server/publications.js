import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Files from '../Files';

Meteor.publish('file', function files() {
  return Files.find({}, {sort: {createdAt: 1}}).cursor;
});

// Note: file.view is also used when editing an existing file.
Meteor.publish('file.view', function fileView(fileId) {
  console.log(fileId);
  check(fileId, String);
  return Files.find().cursor;
});