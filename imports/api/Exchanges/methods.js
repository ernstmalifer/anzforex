import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Exchanges from './Exchanges';
import rateLimit from '../../modules/rate-limit';

Meteor.methods({
  'exchange.insert': function exchangeInsert(exchange) {

    check(exchange, {
      name: String,
      date: String,
      exchanges: Array,
      // active: Boolean,
    });


    Meteor.call('generatePreview', exchange.date);

    let exchangeBody = `<table><thead><tr><th>Currency</th><th>Buying</th><th>Selling</th></tr></thead><tbody>`;
    exchange.exchanges.forEach(exchange => {
      if(exchange.enable === 'on'){
        exchangeBody += `<tr><th>${exchange.symbol}</th><th>${exchange.buying}</th><th>${exchange.selling}</th></tr>`;
      }
    })
    exchangeBody += `</tbody></table><p>Please don't respond to this email<p>`

    try {

      // console.log(exchangeBody);

      Meteor.setTimeout(function(){
        Meteor.call('updatedExchange', 
          'Ernst <ernst.malifer@engagis.com>', 
          `New Foreign Exchange for ${exchange.date}!`, 
          exchangeBody
        );
      }, 1000); // 15 s

      // Exchanges.remove({date : {$eq: exchange.date}});
      
      return Exchanges.insert({ owner: this.userId, ...exchange });
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'exchange.update': function exchangeUpdate(exchange) {
    check(exchange, {
      _id: String,
      name: String,
      date: String,
      exchanges: Array,
    });

    try {
      const exchangeId = exchange._id;
      Exchanges.update(exchangeId, { $set: exchange });
      return exchangeId; // Return _id so we can redirect to exchange after update.
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'exchange.remove': function exchangeRemove(exchangeId) {
    check(exchangeId, String);

    try {
      return Exchanges.remove(exchangeId);
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
});

rateLimit({
  methods: [
    'exchange.insert',
    'exchange.update',
    'exchange.remove',
  ],
  limit: 5,
  timeRange: 1000,
});
