import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Exchanges from '../Exchanges';
import moment from 'moment';

Meteor.publish('exchanges', function exchange() {
  return Exchanges.find();
}, {
  url: 'api/exchanges'
});

// Note: exchange.view is also used when editing an existing exchange.
Meteor.publish('exchanges.view', function exchangeView(exchangeId) {
  check(exchangeId, String);
  return Exchanges.find({_id: exchangeId});
}, {
  url: 'api/exchanges/:0'
});

Meteor.publish('exchanges.date', function exchangeView(date) {
  check(date, String);
  return Exchanges.find({date: date});
}, {
  url: 'api/exchanges/date/:0'
});

Meteor.publish('exchanges.today', function exchangeView() {
  const today = moment().format('YYYY-MM-DD'); // server date
  return Exchanges.find({date: today});
}, {
  url: 'api/exchanges/today'
});

Meteor.publish('exchanges.recent', function exchangeView() {
  // console.log('get recent');
  return Exchanges.find({}, {limit: 1, sort: {date: -1, createdAt: -1}});
}, {
  url: 'api/exchanges/recent'
});
