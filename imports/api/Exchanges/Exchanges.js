/* eslint-disable consistent-return */

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Exchanges = new Mongo.Collection('Exchanges');

Exchanges.allow({
  insert: () => true,
  update: () => false,
  remove: () => false,
});

Exchanges.deny({
  insert: () => false,
  update: () => true,
  remove: () => true,
});

Exchanges.schema = new SimpleSchema({
  owner: {
    type: String,
    label: 'The ID of the user this exchange belongs to.',
  },
  createdAt: {
    type: String,
    label: 'The date this exchange was created.',
    autoValue() {
      if (this.isInsert) return (new Date()).toISOString();
    },
  },
  updatedAt: {
    type: String,
    label: 'The date this currency was last updated.',
    autoValue() {
      if (this.isInsert || this.isUpdate) return (new Date()).toISOString();
    },
  },
  name: {
    type: String,
    label: 'The name of the exchange.',
  },
  date: {
    type: String,
    label: 'The date of the exchange.',
  },
  exchanges: {
    type: Array,
    label: 'The exchanges of the exchange',
  },
  'exchanges.$': {
    type: Object
  },
  'exchanges.$._id': {
    type: String
  },
  'exchanges.$.name': {
    type: String
  },
  'exchanges.$.country': {
    type: String
  },
  'exchanges.$.symbol': {
    type: String
  },
  'exchanges.$.image': {
    type: String
  },
  'exchanges.$.enable': {
    type: String
  },
  'exchanges.$.buying': {
    type: Number
  },
  'exchanges.$.selling': {
    type: Number
  },
  // active: {
  //   type: Boolean,
  //   label: 'The state of exchange'
  // },
});

Exchanges.attachSchema(Exchanges.schema);

export default Exchanges;
