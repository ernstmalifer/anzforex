import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Uploads from '../Uploads';

Meteor.publish('upload', function upload() {
  return Uploads.find({}, {sort: {createdAt: -1}}).cursor;
});

// Note: file.view is also used when editing an existing file.
Meteor.publish('upload.view', function uploadView(fileId) {
  console.log(fileId);
  check(fileId, String);
  return Uploads.find().cursor;
});