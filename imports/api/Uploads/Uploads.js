import { Meteor } from 'meteor/meteor';
import { FilesCollection } from 'meteor/ostrio:files';

const Uploads = new FilesCollection({
  debug: false,
  collectionName: 'Uploads',
  // storagePath: Meteor.absolutePath +'/public/content/images', 
  public: true,
  downloadRoute: '/cdn/uploads',
  allowClientCode: true, // Disallow remove files from Client
  onBeforeUpload(file) {
    // Allow upload files under 10MB, and only in png/jpg/jpeg formats
    if (file.size <= 10485760) {
      // console.log(process.cwd());
      return true;
    } else {
      return 'Please upload file, with size equal or less than 10MB';
    }
  }
});

export default Uploads;