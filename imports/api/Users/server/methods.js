import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Accounts } from 'meteor/accounts-base';
import editProfile from './edit-profile';
import rateLimit from '../../../modules/rate-limit';
import { Email } from 'meteor/email';

var Horseman = require('node-horseman');

Meteor.methods({
  'users.addUser': function usersAddUser(email, password, profile){

    check(email, String);
    check(password, String);
    check(profile, {
      name: {
        first: String,
        last: String,
      },
      notifications: {
        newexchange: Boolean,
      },
      defaultpassword: String,
    });

    const id = Accounts.createUser({
      email: email,
      password: password,
      profile: profile,
    });

    Accounts.sendVerificationEmail(id);
    return id;

  },
  'users.sendVerificationEmail': function usersResendVerification() {
    return Accounts.sendVerificationEmail(this.userId);
  },
  'users.editProfile': function usersEditProfile(profile) {
    check(profile, {
      emailAddress: String,
      profile: {
        name: {
          first: String,
          last: String,
        },
        notifications: {
          newexchange: Boolean
        },
      },
    });

    return editProfile({ userId: this.userId, profile })
    .then(response => response)
    .catch((exception) => {
      throw new Meteor.Error('500', exception);
    });
  },
  'updatedExchange': function updatedExchange(from, subject, text) {
    
    check([from, subject, text], [String]);

    const tos = Meteor.users.find({}).fetch();

    const recipients = [];
    tos.forEach((to) => {
      if(to.profile.notifications.newexchange) {
        recipients.push(`${to.profile.name.first} ${to.profile.name.last} <${to.emails[0].address}>`);
      }
    });

    this.unblock();


    try {
      // Email.send({ to, from, subject, text, attachments: [{filename: 'big.png', path: 'big.png'}] });
      Email.send({ to: recipients, from: from, subject: subject, html: text });
    } catch(exception) {
      throw new Meteor.Error('500', exception);
    }

  },
  'generatePreview': function generatePreview(date) {

    check(date, String);

    console.log(Meteor.absoluteUrl() + 'preview/' + date);

    var horseman = new Horseman({timeout: 10000});

    horseman
      .viewport(1080,1920)
      .open(Meteor.absoluteUrl() + 'preview/' + date)
      .wait(5000)
      .screenshot('big.png')
      .close();
  },
  'users.removeUser': function usersRemoveUser(_id) {
    check(_id, String);
    return Meteor.users.remove(_id);
  }
});

rateLimit({
  methods: [
    'users.sendVerificationEmail',
    'users.editProfile',
  ],
  limit: 5,
  timeRange: 1000,
});