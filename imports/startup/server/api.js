// import '../../api/Documents/methods';
// import '../../api/Documents/server/publications';

import '../../api/Files/methods';
import '../../api/Files/server/publications';

import '../../api/Uploads/methods';
import '../../api/Uploads/server/publications';

import '../../api/Currency/methods';
import '../../api/Currency/server/publications';

import '../../api/Exchanges/methods';
import '../../api/Exchanges/server/publications';

import '../../api/OAuth/server/methods';

import '../../api/Users/server/methods';
import '../../api/Users/server/publications';

import '../../api/Utility/server/methods';
