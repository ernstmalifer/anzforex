import { SimpleRest } from 'meteor/simple:rest';
import { JsonRoutes } from 'meteor/simple:json-routes';
 
Meteor.startup(() => {
  JsonRoutes.setResponseHeaders({
    'Cache-Control': 'no-store',
    Pragme: 'no-cache',
    'Access-Control-Allow-Orgin': '*',
    'Access-Control-Allow-Methods': 'GET',
    'Access-Control-Allow-Headers': 'Content-Type, Authrorization, X-Requested-With',
  });
});
