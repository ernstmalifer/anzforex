/* eslint-disable jsx-a11y/no-href*/

import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Grid, Alert, Button } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Roles } from 'meteor/alanning:roles';
import { Bert } from 'meteor/themeteorchef:bert';
import Navigation from '../../components/Navigation/Navigation';
import Authenticated from '../../components/Authenticated/Authenticated';
import Public from '../../components/Public/Public';
import Index from '../../pages/Index/Index';
// import Documents from '../../pages/Documents/Documents';
// import NewDocument from '../../pages/NewDocument/NewDocument';
// import ViewDocument from '../../pages/ViewDocument/ViewDocument';
// import EditDocument from '../../pages/EditDocument/EditDocument';
import Exchanges from '../../pages/Exchanges/Exchanges';
import NewExchange from '../../pages/NewExchange/NewExchange';
import ViewExchange from '../../pages/ViewExchange/ViewExchange';
import EditExchange from '../../pages/EditExchange/EditExchange';
import EditExchangeCopy from '../../pages/EditExchangeCopy/EditExchangeCopy';
import Currencies from '../../pages/Currencies/Currencies';
import NewCurrency from '../../pages/NewCurrency/NewCurrency';
import ViewCurrency from '../../pages/ViewCurrency/ViewCurrency';
import EditCurrency from '../../pages/EditCurrency/EditCurrency';
import Files from '../../pages/Files/Files';
import NewFile from '../../pages/NewFile/NewFile';
import ViewFile from '../../pages/ViewFile/ViewFile';
import EditFile from '../../pages/EditFile/EditFile';
import Uploads from '../../pages/Uploads/Uploads';
import Users from '../../pages/Users/Users';
import NewUser from '../../pages/NewUser/NewUser';
import Signup from '../../pages/Signup/Signup';
import Login from '../../pages/Login/Login';
import Logout from '../../pages/Logout/Logout';
import VerifyEmail from '../../pages/VerifyEmail/VerifyEmail';
import RecoverPassword from '../../pages/RecoverPassword/RecoverPassword';
import ResetPassword from '../../pages/ResetPassword/ResetPassword';
import Profile from '../../pages/Profile/Profile';
import NotFound from '../../pages/NotFound/NotFound';
import Footer from '../../components/Footer/Footer';
import Terms from '../../pages/Terms/Terms';
import Privacy from '../../pages/Privacy/Privacy';
import ExamplePage from '../../pages/ExamplePage/ExamplePage';
import Preview from '../../pages/Preview/Preview';

import './App.scss';

const handleResendVerificationEmail = (emailAddress) => {
  Meteor.call('users.sendVerificationEmail', (error) => {
    if (error) {
      Bert.alert(error.reason, 'danger');
    } else {
      Bert.alert(`Check ${emailAddress} for a verification link!`, 'success');
    }
  });
};

const App = props => (
  <Router>
      <Switch>
        <Route exact path="/preview/:date" component={Preview}/> 
    {!props.loading ? <div className="App">
      {props.userId && !props.emailVerified ? <Alert className="verify-email text-center"><p>Hey there! Can you <strong>verify your email address</strong> ({props.emailAddress}) for us? <Button bsStyle="link" onClick={() => handleResendVerificationEmail(props.emailAddress)} href="#">Re-send verification email</Button></p></Alert> : ''}
        <Navigation {...props} />
        <Grid>
          <Switch>
            <Route exact name="index" path="/" component={Index} />
            {/* <Authenticated exact path="/documents" component={Documents} {...props} />
            <Authenticated exact path="/documents/new" component={NewDocument} {...props} />
            <Authenticated exact path="/documents/:_id" component={ViewDocument} {...props} />
            <Authenticated exact path="/documents/:_id/edit" component={EditDocument} {...props} /> */}
            <Authenticated exact path="/exchanges" component={Exchanges} {...props} />
            <Authenticated exact path="/exchanges/new" component={NewExchange} {...props} />
            <Authenticated exact path="/exchanges/:_id" component={ViewExchange} {...props} />
            <Authenticated exact path="/exchanges/:_id/edit" component={EditExchange} {...props} />
            <Authenticated exact path="/exchanges/:_id/copy" component={EditExchangeCopy} {...props} />
            <Authenticated exact path="/currencies" component={Currencies} {...props} />
            <Authenticated exact path="/currencies/new" component={NewCurrency} {...props} />
            <Authenticated exact path="/currencies/:_id" component={ViewCurrency} {...props} />
            <Authenticated exact path="/currencies/:_id/edit" component={EditCurrency} {...props} />
            <Authenticated exact path="/profile" component={Profile} {...props} />
            <Authenticated exact path="/files" component={Files} {...props} />
            <Authenticated exact path="/files/new" component={NewFile} {...props} />
            <Authenticated exact path="/files/:_id" component={ViewFile} {...props} />
            <Authenticated exact path="/files/:_id/edit" component={EditFile} {...props} />
            <Authenticated exact path="/uploads" component={Uploads} {...props} />
            <Authenticated exact path="/users" component={Users} {...props} />
            <Authenticated exact path="/users/new" component={NewUser} {...props} />
            <Public path="/signup" component={Signup} {...props} /> 
            <Public path="/login" component={Login} {...props} />
            <Public path="/logout" component={Logout} {...props} />
            <Route name="verify-email" path="/verify-email/:token" component={VerifyEmail} />
            <Route name="recover-password" path="/recover-password" component={RecoverPassword} />
            <Route name="reset-password" path="/reset-password/:token" component={ResetPassword} />
            <Route name="terms" path="/terms" component={Terms} />
            <Route name="privacy" path="/privacy" component={Privacy} />
            <Route name="examplePage" path="/example-page" component={ExamplePage} />
            <Route component={NotFound} />
          </Switch>
        </Grid>
        <Footer />
    </div> : ''}
    </Switch>
  </Router>
);

App.defaultProps = {
  userId: '',
  emailAddress: '',
};

App.propTypes = {
  loading: PropTypes.bool.isRequired,
  userId: PropTypes.string,
  emailAddress: PropTypes.string,
  emailVerified: PropTypes.bool.isRequired,
};

const getUserName = name => ({
  string: name,
  object: `${name.first} ${name.last}`,
}[typeof name]);

export default createContainer(() => {
  const loggingIn = Meteor.loggingIn();
  const user = Meteor.user();
  const userId = Meteor.userId();
  const loading = !Roles.subscription.ready();
  const name = user && user.profile && user.profile.name && getUserName(user.profile.name);
  const emailAddress = user && user.emails && user.emails[0].address;

  return {
    loading,
    loggingIn,
    authenticated: !loggingIn && !!userId,
    name: name || emailAddress,
    roles: !loading && Roles.getRolesForUser(userId),
    userId,
    emailAddress,
    emailVerified: user && user.emails ? user && user.emails && user.emails[0].verified : true,
  };
}, App);
