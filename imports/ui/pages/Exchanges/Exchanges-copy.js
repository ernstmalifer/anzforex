import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import ExchangesCollection from '../../../api/Exchanges/Exchanges';
// import FilesCollection from '../../../api/Files/Files';
import Loading from '../../components/Loading/Loading';
import _ from 'lodash';

import './Exchanges.scss';

const handleRemove = (exchangeId) => {
  if (confirm('Are you sure? This is permanent!')) {
    Meteor.call('exchange.remove', exchangeId, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Exchange deleted!', 'success');
      }
    });
  }
};

const Exchanges = ({ loading, exchanges, loadingUsers, users, match, history }) => {

  let groupedExchangeByDate = _.chain(exchanges)
        .groupBy(x => x.date)
        .map((value, key) => ({date: key, exchanges: value}))
        .value();
  
  return (!loading && !loadingUsers ? (
  <div className="Exchanges">
    <div className="page-header clearfix">
      <h4 className="pull-left">Exchanges</h4>
      <div className="pull-right">
        <Link className="btn btn-success" to={`${match.url}/new`}>Add Exchange</Link>
      </div>
    </div>

    
    {exchanges.length ? <div>
      {groupedExchangeByDate.map((date, idx) => {
        return(<div key={`id-${idx}-${date.date}`}>
          <h5><strong>{new Date(date.date).toDateString()}</strong></h5>
          <Table responsive>
          <thead>
            <tr>
              {/* <th>Name</th> */}
              {/* <th>Exchange Date <span className="caret"></span></th> */}
              {/* <th>Last Updated</th> */}
              <th>Created</th>
              <th>Author</th>
              {/* <th />
              <th /> */}
              <th />
            </tr>
          </thead>
          <tbody>
            {date.exchanges.map(({ _id, name, date, createdAt, owner, updatedAt }) => {
              return (<tr key={_id}  id={`exchange-${_id}`} className={new Date(date).toDateString() === new Date().toDateString() ? 'info' : ''}>
                {/* <td>
                  <h5>{name}</h5>
                </td>
                <td>
                  {new Date(date).toDateString()} {new Date(date).toDateString() === new Date().toDateString() ? (<span className="label label-primary">Today</span>) : ''}
                </td> */}
                {/* <td>{timeago(updatedAt)}</td> */}
                <td>{monthDayYearAtTime(createdAt)}</td>
                <td>
                  {users.map((user, idx) => {
                    if(user._id === owner){
                      return (<span key={user._id}>{user.profile.name.first} {user.profile.name.last}</span>)
                    }
                  })}
                </td>
                {/* <td>
                  <Button
                    bsStyle="primary"
                    onClick={() => history.push(`${match.url}/${_id}`)}
                    block
                  >View</Button>
                </td> */}
                {/* <td>
                  <Button
                    bsStyle="info"
                    onClick={() => history.push(`${match.url}/${_id}/copy`)}
                    block
                  >Copy</Button>
                </td> */}
                <td>
                  <Button
                    bsStyle="danger"
                    onClick={() => handleRemove(_id)}
                    block
                  >Delete</Button>
                </td>
              </tr>
            )}
            )}
          </tbody>
        </Table>
        </div>)
      })}
  </div> : <Alert bsStyle="warning">No exchanges yet!</Alert>}
  </div>
) : <Loading />)};

Exchanges.propTypes = {
  loading: PropTypes.bool.isRequired,
  exchanges: PropTypes.arrayOf(PropTypes.object).isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('exchanges');
  const usersSubscription = Meteor.subscribe('users');
  // const filesSubscription = Meteor.subscribe('file');

  return {
    loading: !subscription.ready(),
    exchanges: ExchangesCollection.find({},{sort: {date: -1}}).fetch(),
    loadingUsers: !usersSubscription.ready(),
    users: Meteor.users.find().fetch(),
    // loadingFiles: !filesSubscription.ready(),
    // files: FilesCollection.find().fetch(),
  };
}, Exchanges);
