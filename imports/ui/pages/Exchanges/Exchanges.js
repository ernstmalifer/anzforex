import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import ExchangesCollection from '../../../api/Exchanges/Exchanges';
// import FilesCollection from '../../../api/Files/Files';
import Loading from '../../components/Loading/Loading';
import _ from 'lodash';

import './Exchanges.scss';

const handleRemove = (exchangeId) => {
  if (confirm('Are you sure? This is permanent!')) {
    Meteor.call('exchange.remove', exchangeId, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Exchange deleted!', 'success');
      }
    });
  }
};

// const Exchanges = ({ loading, exchanges, loadingUsers, users, match, history }) => {
class Exchanges extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      showDupes: false
    }
  }

  render(){

      let groupedExchangeByDate = _.orderBy(this.props.exchanges, ['date','createdAt'], ['desc', 'desc']);

      if(!this.state.showDupes){
        groupedExchangeByDate = _.uniqBy(groupedExchangeByDate, (e) => { return e.date; })
      }
      
      return (!this.props.loading && !this.props.loadingUsers ? (
      <div className="Exchanges">
        <div className="page-header clearfix">
          <h4 className="pull-left">Exchanges</h4>
          <div className="pull-right">
            <a className={`btn btn-default ${this.state.showDupes ? `active` : ``}`} onClick={(e) => {this.setState({showDupes: !this.state.showDupes}) }}>Show/Hide Duplicates</a>&nbsp;
            <Link className="btn btn-success" to={`${this.props.match.url}/new`}>Add Exchange</Link>
          </div>
        </div>
    
        
        {groupedExchangeByDate.length ? <Table responsive>
          <thead>
            <tr>
              <th>Name</th>
              <th>Exchange Date <span className="caret"></span></th>
              {/* <th>Last Updated</th> */}
              <th>Created</th>
              <th>Author</th>
              {/* <th />
              <th /> */}
              <th />
            </tr>
          </thead>
          <tbody>
            {groupedExchangeByDate.map(({ _id, name, date, createdAt, owner, updatedAt }, idx) => {

              let dupe = false;
              if(idx > 0) {
                if(groupedExchangeByDate[idx - 1].date === date) {
                  dupe = true;
                }
              }

              return (<tr key={_id}  id={`exchange-${_id}`} className={`${dupe ? `danger` : ``} ${new Date(date).toDateString() === new Date().toDateString() ? 'info' : ''}`}>
                <td>
                  <h5>{name}</h5>
                </td>
                <td>
                  {new Date(date).toDateString()} {new Date(date).toDateString() === new Date().toDateString() ? (<span className="label label-primary">Today</span>) : ''}
                </td>
                {/* <td>{timeago(updatedAt)}</td> */}
                <td>{monthDayYearAtTime(createdAt)}</td>
                <td>
                  {this.props.users.map((user, idx) => {
                    if(user._id === owner){
                      return (<span key={user._id}>{user.profile.name.first} {user.profile.name.last}</span>)
                    }
                  })}
                </td>
                {/* <td>
                  <Button
                    bsStyle="primary"
                    onClick={() => history.push(`${match.url}/${_id}`)}
                    block
                  >View</Button>
                </td> */}
                {/* <td>
                  <Button
                    bsStyle="info"
                    onClick={() => history.push(`${match.url}/${_id}/copy`)}
                    block
                  >Copy</Button>
                </td> */}
                <td>
                  <Button
                    bsStyle="danger"
                    onClick={() => handleRemove(_id)}
                    block
                  >Delete</Button>
                </td>
              </tr>
            )}
            )}
          </tbody>
        </Table> : <Alert bsStyle="warning">No exchanges yet!</Alert>}
    
      
    
      </div>
    ) : <Loading />)
  }

};

Exchanges.propTypes = {
  loading: PropTypes.bool.isRequired,
  exchanges: PropTypes.arrayOf(PropTypes.object).isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('exchanges');
  const usersSubscription = Meteor.subscribe('users');
  // const filesSubscription = Meteor.subscribe('file');

  return {
    loading: !subscription.ready(),
    exchanges: ExchangesCollection.find({},{sort: {date: -1}}).fetch(),
    loadingUsers: !usersSubscription.ready(),
    users: Meteor.users.find().fetch(),
    // loadingFiles: !filesSubscription.ready(),
    // files: FilesCollection.find().fetch(),
  };
}, Exchanges);
