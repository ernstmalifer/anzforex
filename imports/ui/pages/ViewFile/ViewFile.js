import React from 'react';
import PropTypes from 'prop-types';
import { ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import FilesCollection from '../../../api/Files/Files';
import NotFound from '../NotFound/NotFound';
import Loading from '../../components/Loading/Loading';

const handleRemove = (fileId, history) => {
  if (confirm('Are you sure? This is permanent!')) {
    // Meteor.call('file.remove', fileId, (error) => {
    //   if (error) {
    //     Bert.alert(error.reason, 'danger');
    //   } else {
    //     Bert.alert('File deleted!', 'success');
    //     history.push('/files');
    //   }
    // });

    FilesCollection.remove({_id: fileId}, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('File deleted!', 'success');
        history.push('/files');
      }
    })
  }
};

const renderFile = (file, match, history) => (file ? (
  <div className="ViewFile">
     <div className="page-header clearfix">
      <h4 className="pull-left">{ file && file.meta.name }</h4>
      <ButtonToolbar className="pull-right">
        <ButtonGroup bsSize="small">
          <Button onClick={() => history.push(`${match.url}/edit`)}>Edit</Button>
          <Button onClick={() => handleRemove(file._id, history)} className="text-danger btn-danger">
            Delete
          </Button>
        </ButtonGroup>
      </ButtonToolbar>
    </div>
    <img src={`url(${FilesCollection.findOne({_id: file._id}).link()})`} width="200" />
  </div>
) : <NotFound />);

const ViewFile = ({ loading, file, match, history }) => (
  !loading ? renderFile(file, match, history) : <Loading />
);

ViewFile.propTypes = {
  loading: PropTypes.bool.isRequired,
  file: PropTypes.object,
  // file: PropTypes.arrayOf(PropTypes.object).isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default createContainer(({ match }) => {
  const fileId = match.params._id;
  const subscription = Meteor.subscribe('file.view', fileId);

  return {
    loading: !subscription.ready(),
    file: FilesCollection.findOne({_id: fileId}),
  };
}, ViewFile);
