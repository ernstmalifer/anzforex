import React from 'react';
import { Button } from 'react-bootstrap';

import './Index.scss';

const Index = () => (
  <div className="Index">
    <div className="col-md-12">
    <div className="jumbotron">
      <div className="hero" style={{backgroundImage: 'url(images/hero.png)'}}></div>
      <div style={{position: 'relative', zIndex: 1}}>
        <div className="row">
          <div className="col-md-8 col-sm-8 col-xs-8">
        <h1>Welcome to ANZ Forex Management</h1>
        <p>This forex microsite allows ANZ Sydney Airport Staff to update foreign exchange rates displayed on the Digital Media Screens (DMS) daily by uploading forex data files. The data will be displayed via the forex HTML template.</p>

        <p>The microsite offers ability and flexibility to publish forex rates into the screens at the ANZ Sydney Airport Branch without manually updating the rates in the DMS. It also gives the uploader a simple preview of the forex rates that will be published on the digital screens. </p>
        <p><a className="btn btn-success btn-lg" href="/exchanges/new" role="button">Create Exchanges</a></p>
      </div>
      </div>
      </div>
    </div>
    </div>
  </div>
);

export default Index;
