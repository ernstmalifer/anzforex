import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import FilesCollection from '../../../api/Files/Files';
import Loading from '../../components/Loading/Loading';

import './Files.scss';

const handleRemove = (e, fileId) => {
  // if (confirm('Are you sure? This is permanent!')) {
  //   Meteor.call('file.remove', fileId, (error) => {
  //     if (error) {
  //       Bert.alert(error.reason, 'danger');
  //     } else {
  //       Bert.alert('File deleted!', 'success');
  //     }
  //   });
  // }
  e.stopPropagation();
  if (confirm('Are you sure? This is permanent!')) {
    FilesCollection.remove({_id: fileId}, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('File deleted!', 'success');
        // history.push('/files');
      }
    })
  }
};

// const Files = ({ files, loadingUsers, users, match, history }) => ( !loadingUsers ? (
class Files extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      view: 'grid'
    }
  }

  switchView = (view) => {
    this.setState({view});
  }

  render() {
    return(
      !this.props.loadingUsers ? (
    <div className="Files">
      <div className="page-header clearfix">
        <h4 className="pull-left">Files</h4>
        <div className="pull-right">
          <button className={`btn ${this.state.view === 'list' ? 'btn-primary' : 'btn-default'}`} onClick={this.switchView.bind(this, 'list')} title="List"><i className="glyphicon glyphicon-th-list" /></button> &nbsp;
          <button className={`btn ${this.state.view === 'grid' ? 'btn-primary' : 'btn-default'}`} onClick={this.switchView.bind(this, 'grid')} title="Grid"><i className="glyphicon glyphicon-th" /></button> &nbsp;
          <Link className="btn btn-success" to={`${this.props.match.url}/new`}>Add File</Link>
        </div>
      </div>

      {this.props.files.length && this.state.view === 'list' ? 
        <Table responsive>
          <thead>
            <tr>
              <th></th>
              <th>Name</th>
              <th>Last Updated</th>
              <th>Created</th>
              <th>Author</th>
              <th />
              <th />
            </tr>
          </thead>
          <tbody>
            {this.props.files.map(({ _id, name, type, size, location, meta }) => {
              return (<tr key={_id}>
                <td><img src={`url(${FilesCollection.findOne({_id: _id}).link()})`} width="50" /></td>
                <td>
                  <h5>{meta.name}</h5>
                  {type}<br />
                  {size}<br />
                </td>
                <td>{timeago(meta.updatedAt)}</td>
                <td>{monthDayYearAtTime(meta.createdAt)}</td>
                <td>
                  {this.props.users.map((user, idx) => {
                    if(user._id === meta.owner){
                      return (<span key={user._id}>{user.profile.name.first} {user.profile.name.last}</span>)
                    }
                  })}
                </td>
                <td>
                  <Button
                    bsStyle="primary"
                    onClick={() => this.props.history.push(`${this.props.match.url}/${_id}`)}
                    block
                  >View</Button>
                </td>
                <td>
                  <Button
                    bsStyle="danger"
                    onClick={(e) => handleRemove(e, _id)}
                    block
                  >Delete</Button>
                </td>
              </tr>
            )}
            )}
          </tbody>
        </Table> 
        : 
        ''
      }
      {this.props.files.length && this.state.view === 'grid' ? 
      <div className="row">
        {this.props.files.map(({_id, meta, type, size}, index) => {
          {/* console.log(FilesCollection.findOne().link()) */}
          {/* return (<img key={index} src={FilesCollection.find({_id: file._id}).link} />) */}
           return (<div key={_id} className="col-md-3 col-lg-2 col-sm-4 col-xs-6">
            <div className="file-thumbnail thumbnail" style={{minHeight: '150px'}} onClick={() => this.props.history.push(`${this.props.match.url}/${_id}`)}>
              <Button bsStyle="danger" className="btn-xs" onClick={(e) => handleRemove(e,_id)}><i className="glyphicon glyphicon-trash"></i></Button>
              <div style={{backgroundImage: `url(${FilesCollection.findOne({_id: _id}).link()})`, backgroundPosition: 'center', backgroundSize: 'contain', backgroundRepeat: 'no-repeat', height: '80px', marginTop: '10px'}} />
              <div style={{padding: '10px'}}>
                <h5>{meta.name}</h5>
                {type}<br />
                {size}
              </div>
            </div>
          </div>) 
        })}
      </div>
      :
        ''
      }
      {this.props.files.length === 0 ? 
        <Alert bsStyle="warning">No files yet!</Alert>
        :
        ''
      }
      </div>
    ) : <Loading />)
  }}

Files.propTypes = {
  // loading: PropTypes.bool.isRequired,
  files: PropTypes.arrayOf(PropTypes.object).isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('file');
  const usersSubscription = Meteor.subscribe('users');

  return {
    loading: !subscription.ready(),
    files: FilesCollection.find().fetch(),
    loadingUsers: !usersSubscription.ready(),
    users: Meteor.users.find().fetch()
  };
}, Files);
