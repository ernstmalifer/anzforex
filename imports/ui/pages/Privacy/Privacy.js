import React from 'react';
import Page from '../Page/Page';

const Privacy = () => (
  <div className="Privacy">
    <Page
      title="Privacy Policy"
      subtitle="Last updated September 14th, 2017"
      page="privacy"
    />
  </div>
);

export default Privacy;
