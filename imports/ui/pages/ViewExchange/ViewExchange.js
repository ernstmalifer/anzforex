import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup, ControlLabel, ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import Exchanges from '../../../api/Exchanges/Exchanges';
import FilesCollection from '../../../api/Files/Files';
import NotFound from '../NotFound/NotFound';
import Loading from '../../components/Loading/Loading';
import moment from 'moment';

import Preview from '../Preview/Preview';

import './ViewExchange.scss';

const handleRemove = (exchangeId, history) => {
  if (confirm('Are you sure? This is permanent!')) {
    Meteor.call('exchange.remove', exchangeId, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Exchange deleted!', 'success');
        history.push('/exchanges');
      }
    });
  }
};

const renderExchange = (exchange, files, match, history) => (exchange ? (
  <div className="ViewExchange">
    <div className="page-header clearfix">
      <h4 className="pull-left">{ exchange && exchange.name }</h4>
      <ButtonToolbar className="pull-right">
        <ButtonGroup bsSize="small">
          <Button onClick={() => history.push(`${match.url}/edit`)}>Edit</Button>
          <Button onClick={() => handleRemove(exchange._id, history)} className="text-danger btn-danger">
            Delete
          </Button>
        </ButtonGroup>
      </ButtonToolbar>
    </div>
    <div className="row">
      <div className="col-md-6">
        <FormGroup>
          <ControlLabel>Exchange Date</ControlLabel>
          <input
            type="date"
            className="form-control"
            name="date"
            value={exchange.date}
            placeholder="Exchange Date"
            readOnly
          />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Exchanges</ControlLabel>
          <div>
          {exchange.exchanges.map((currency, index) => {
            if (currency.enable === 'on'){
              return (
                <div key={`exchange-${exchange._id}-${index}`} className="panel panel-default">
                  <div className="panel-body">
                    <div className="row">
                      <div className="col-xs-1 text-center">
                        <input type="checkbox" checked={currency.enable === 'on' ? 'checked' : ''} readOnly/>
                      </div>
                      <div className="col-xs-5">
                        {files.map((file, index) => {
                          if(currency.image === file._id){
                            return (<img key={`file-${file._id}`} className="currency-image" src={`url(${FilesCollection.findOne({_id: file._id}).link()})`} width="30" />);
                          } else {
                            return '';
                          }
                        })}
                        {currency.name}
                      </div>
                      <div className="col-xs-3">
                        <input type="number" className="form-control input-sm" placeholder="Buying" value={currency.buying} readOnly />
                      </div>
                      <div className="col-xs-3">
                        <input type="number" className="form-control input-sm" placeholder="Selling" value={currency.selling} readOnly />
                      </div>
                    </div>
                  </div>
                </div>
              )
            } else {
              return '';
            }
          })}
          </div>
        </FormGroup>
      </div>
      <div className="col-md-6">
        <Preview exchange={exchange} history={history} rescale={true} />
      </div>
    </div>
  </div>
) : <NotFound />);

const ViewExchange = ({ loading, loadingFiles, exchange, files, match, history }) => (
  !loading && !loadingFiles ? renderExchange(exchange, files, match, history) : <Loading />
);

ViewExchange.propTypes = {
  loading: PropTypes.bool.isRequired,
  exchange: PropTypes.object,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default createContainer(({ match }) => {
  const exchangeId = match.params._id;
  const subscription = Meteor.subscribe('exchanges.view', exchangeId);
  const filesSubscription = Meteor.subscribe('file');

  return {
    loading: !subscription.ready(),
    exchange: Exchanges.findOne(exchangeId),
    loadingFiles: !filesSubscription.ready(),
    files: FilesCollection.find().fetch(),
  };
}, ViewExchange);
