import React from 'react';
import PropTypes from 'prop-types';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import Exchanges from '../../../api/Exchanges/Exchanges';
import ExchangeEditor from '../../components/ExchangeEditor/ExchangeEditor';
import NotFound from '../NotFound/NotFound';

const EditExchange = ({ exchange, history }) => (exchange ? (
  <div className="EditExchange">
    <h4 className="page-header">{`Editing "${exchange.name}"`}</h4>
    <ExchangeEditor exchange={exchange} history={history} />
  </div>
) : <NotFound />);

EditExchange.defaultProps = {
  exchange: null,
};

EditExchange.propTypes = {
  exchange: PropTypes.object,
  history: PropTypes.object.isRequired,
};

export default createContainer(({ match }) => {
  const exchangeId = match.params._id;
  const subscription = Meteor.subscribe('exchanges.view', exchangeId);

  return {
    loading: !subscription.ready(),
    exchange: Exchanges.findOne(exchangeId),
  };
}, EditExchange);
