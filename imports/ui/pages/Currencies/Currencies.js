import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import CurrencyCollection from '../../../api/Currency/Currency';
import FilesCollection from '../../../api/Files/Files';
import Loading from '../../components/Loading/Loading';

import './Currencies.scss';

const handleRemove = (currencyId) => {
  if (confirm('Are you sure? This is permanent!')) {
    Meteor.call('currency.remove', currencyId, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Currency deleted!', 'success');
      }
    });
  }
};

const Currencies = ({ loading, currencies, loadingUsers, loadingFiles, users, files, match, history }) => (!loading && !loadingUsers && !loadingFiles ? (
  <div className="Currencies">
    <div className="page-header clearfix">
      <h4 className="pull-left">Currencies</h4>
      <Link className="btn btn-success pull-right" to={`${match.url}/new`}>Add Currency</Link>
    </div>

    {currencies.length ? <Table responsive>
      <thead>
        <tr>
          <th></th>
          <th>Name</th>
          <th>Last Updated</th>
          <th>Created</th>
          <th>Author</th>
          <th />
          <th />
        </tr>
      </thead>
      <tbody>
        {currencies.map(({ _id, name, country, symbol, createdAt, image, owner, updatedAt }) => {
          return (<tr key={_id}>
            <td>
              {files.map((file, idx) => {
                if(file._id === image){
                  return (<img key={file._id} src={`url(${FilesCollection.findOne({_id: file._id}).link()})`} width="50" />)
                }
              })}
            </td>
            <td>
              <h5>{name}</h5>
              {country}<br />
              {symbol}
            </td>
            <td>{timeago(updatedAt)}</td>
            <td>{monthDayYearAtTime(createdAt)}</td>
            <td>
              {users.map((user, idx) => {
                if(user._id === owner){
                  return (<span key={user._id}>{user.profile.name.first} {user.profile.name.last}</span>)
                }
              })}
            </td>
            <td>
              <Button
                bsStyle="primary"
                onClick={() => history.push(`${match.url}/${_id}`)}
                block
              >View</Button>
            </td>
            <td>
              <Button
                bsStyle="danger"
                onClick={() => handleRemove(_id)}
                block
              >Delete</Button>
            </td>
          </tr>
        )}
        )}
      </tbody>
    </Table> : <Alert bsStyle="warning">No currencies yet!</Alert>}
  </div>
) : <Loading />);

Currencies.propTypes = {
  loading: PropTypes.bool.isRequired,
  currencies: PropTypes.arrayOf(PropTypes.object).isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('currency');
  const usersSubscription = Meteor.subscribe('users');
  const filesSubscription = Meteor.subscribe('file');

  return {
    loading: !subscription.ready(),
    currencies: CurrencyCollection.find().fetch(),
    loadingUsers: !usersSubscription.ready(),
    users: Meteor.users.find().fetch(),
    loadingFiles: !filesSubscription.ready(),
    files: FilesCollection.find().fetch(),
  };
}, Currencies);
