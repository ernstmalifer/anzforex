import React from 'react';
import PropTypes from 'prop-types';
import ExchangeEditor from '../../components/ExchangeEditor/ExchangeEditor';

const NewCurrency = ({ history }) => (
  <div className="NewExchange">
    <h4 className="page-header">New Exchange</h4>
    <ExchangeEditor history={history} />
  </div>
);

NewCurrency.propTypes = {
  history: PropTypes.object.isRequired,
};

export default NewCurrency;
