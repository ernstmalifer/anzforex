import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import UploadsCollection from '../../../api/Uploads/Uploads';
import Loading from '../../components/Loading/Loading';
import _ from 'lodash';

import './Uploads.scss';

const handleRemove = (e, fileId) => {
  // if (confirm('Are you sure? This is permanent!')) {
  //   Meteor.call('file.remove', fileId, (error) => {
  //     if (error) {
  //       Bert.alert(error.reason, 'danger');
  //     } else {
  //       Bert.alert('File deleted!', 'success');
  //     }
  //   });
  // }
  e.stopPropagation();
  if (confirm('Are you sure? This is permanent!')) {
    UploadsCollection.remove({_id: fileId}, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('File deleted!', 'success');
        // history.push('/files');
      }
    })
  }
};

// const Uploads = ({ uploads, loadingUsers, users, match, history }) => ( !loadingUsers ? (
class Uploads extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      view: 'list'
    }
  }

  switchView = (view) => {
    this.setState({view});
  }

  render() {

    let sortedUploads = _.sortBy(this.props.uploads, (upload) => { return upload.createdAt; }).reverse();

    return(
      !this.props.loadingUsers ? (
    <div className="Uploads">
      <div className="page-header clearfix">
        <h4 className="pull-left">Uploads</h4>
        <div className="pull-right">
          <button className={`btn ${this.state.view === 'list' ? 'btn-primary' : 'btn-default'}`} onClick={this.switchView.bind(this, 'list')} title="List"><i className="glyphicon glyphicon-th-list" /></button> &nbsp;
          <button className={`btn ${this.state.view === 'grid' ? 'btn-primary' : 'btn-default'}`} onClick={this.switchView.bind(this, 'grid')} title="Grid"><i className="glyphicon glyphicon-th" /></button> &nbsp;
          {/* <Link className="btn btn-success" to={`${this.props.match.url}/new`}>Add File</Link> */}
        </div>
      </div>

      {sortedUploads.length && this.state.view === 'list' ? 
        <Table responsive>
          <thead>
            <tr>
              <th></th>
              <th>Name</th>
              <th>Last Updated</th>
              <th>Created</th>
              <th>Author</th>
              <th />
              {/* <th /> */}
            </tr>
          </thead>
          <tbody>
            {sortedUploads.map(({ _id, name, type, size, location, meta }) => {
              return (<tr key={_id}>
                <td><img src={`images/xls.png`} width="50" /></td>
                <td>
                  <h5>{meta.name}</h5>
                  {type}<br />
                  {size}<br />
                </td>
                <td>{timeago(meta.updatedAt)}</td>
                <td>{monthDayYearAtTime(meta.createdAt)}</td>
                <td>
                  {this.props.users.map((user, idx) => {
                    if(user._id === meta.owner){
                      return (<span key={user._id}>{user.profile.name.first} {user.profile.name.last}</span>)
                    }
                  })}
                </td>
                <td>
                  <Button
                    bsStyle="primary"
                    onClick={() => window.open(`${UploadsCollection.findOne({_id: _id}).link()}?download=true`)}
                    block
                  >Download</Button>
                </td>
                {/* <td>
                  <Button
                    bsStyle="danger"
                    onClick={(e) => handleRemove(e, _id)}
                    block
                  >Delete</Button>
                </td> */}
              </tr>
            )}
            )}
          </tbody>
        </Table> 
        : 
        ''
      }
      {this.props.uploads.length && this.state.view === 'grid' ? 
      <div className="row">
        {this.props.uploads.map(({_id, meta, type, size}, index) => {
          {/* console.log(FilesCollection.findOne().link()) */}
          {/* return (<img key={index} src={FilesCollection.find({_id: file._id}).link} />) */}
           return (<div key={_id} className="col-md-3 col-lg-2 col-sm-4 col-xs-6">
            <div className="file-thumbnail thumbnail" style={{minHeight: '150px'}} onClick={() => window.open(`${UploadsCollection.findOne({_id: _id}).link()}?download=true`)}>
              {/* <Button bsStyle="danger" className="btn-xs" onClick={(e) => handleRemove(e,_id)}><i className="glyphicon glyphicon-trash"></i></Button> */}
              <div style={{backgroundImage: `url(images/xls.png)`, backgroundPosition: 'center', backgroundSize: 'contain', backgroundRepeat: 'no-repeat', height: '80px', marginTop: '15px'}} />
              <div style={{padding: '10px'}}>
                <h5>{meta.name}</h5>
              </div>
            </div>
          </div>) 
        })}
      </div>
      :
        ''
      }
      {this.props.uploads.length === 0 ? 
        <Alert bsStyle="warning">No uploads yet!</Alert>
        :
        ''
      }
      </div>
    ) : <Loading />)
  }}

Uploads.propTypes = {
  // loading: PropTypes.bool.isRequired,
  uploads: PropTypes.arrayOf(PropTypes.object).isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('upload');
  const usersSubscription = Meteor.subscribe('users');

  return {
    loading: !subscription.ready(),
    uploads: UploadsCollection.find({}, {sort: {createdAt: -1}}).fetch(),
    loadingUsers: !usersSubscription.ready(),
    users: Meteor.users.find().fetch()
  };
}, Uploads);
