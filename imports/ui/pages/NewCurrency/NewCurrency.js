import React from 'react';
import PropTypes from 'prop-types';
import CurrencyEditor from '../../components/CurrencyEditor/CurrencyEditor';

const NewCurrency = ({ history }) => (
  <div className="NewCurrency">
    <h4 className="page-header">New Currency</h4>
    <CurrencyEditor history={history} />
  </div>
);

NewCurrency.propTypes = {
  history: PropTypes.object.isRequired,
};

export default NewCurrency;

