import React from 'react';
import PropTypes from 'prop-types';
import FileEditor from '../../components/FileEditor/FileEditor';

const NewFile = ({ history }) => (
  <div className="NewFile">
    <h4 className="page-header">New File</h4>
    <FileEditor history={history} />
  </div>
);

NewFile.propTypes = {
  history: PropTypes.object.isRequired,
};

export default NewFile;
