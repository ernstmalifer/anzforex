import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import Loading from '../../components/Loading/Loading';

import './Users.scss';

const handleRemove = (e, userId) => {
  // if (confirm('Are you sure? This is permanent!')) {
  //   Meteor.call('file.remove', fileId, (error) => {
  //     if (error) {
  //       Bert.alert(error.reason, 'danger');
  //     } else {
  //       Bert.alert('File deleted!', 'success');
  //     }
  //   });
  // }
  e.stopPropagation();
  if (confirm('Are you sure? This is permanent!')) {
    Meteor.call('users.removeUser', userId, (error, result) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('User Removed!', 'success');
      }
    })
  }
};

class Users extends React.Component {

  constructor(props) {
    super(props);
  }

  switchView = (view) => {
    this.setState({view});
  }

  render() {
    return(
      !this.props.loadingUsers ? (
    <div className="Users">
      <div className="page-header clearfix">
        <h4 className="pull-left">Users</h4>
        <div className="pull-right">
          <Link className="btn btn-success" to={`${this.props.match.url}/new`}>Add User</Link>
        </div>
      </div>

      {this.props.users.length ? 
        <Table responsive>
          <thead>
            <tr>
              <th>Name</th>
              <th>E-Mail</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {this.props.users.map(({ _id, emails, profile }) => {
              return (<tr key={_id} id={`user-${_id}`}>
                <td>
                  <h5>{profile.name.first} {profile.name.last}</h5>
                </td>
                <td>
                  {emails[0].address} {emails[0].verified}
                </td>
                <td>
                  <Button
                    bsStyle="danger"
                    onClick={(e) => handleRemove(e, _id)}
                    block
                  >Delete</Button>
                </td>
              </tr>
            )}
            )}
          </tbody>
        </Table> 
        : 
        ''
      }
      
      {this.props.users.length === 0 ? 
        <Alert bsStyle="warning">No Users yet!</Alert>
        :
        ''
      }
      </div>
    ) : <Loading />)
  }}

  Users.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default createContainer(() => {
  const usersSubscription = Meteor.subscribe('users');

  return {
    loadingUsers: !usersSubscription.ready(),
    users: Meteor.users.find().fetch()
  };
}, Users);
