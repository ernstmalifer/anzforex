import React from 'react';
import PropTypes from 'prop-types';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import Currency from '../../../api/Currency/Currency';
import CurrencyEditor from '../../components/CurrencyEditor/CurrencyEditor';
import NotFound from '../NotFound/NotFound';

const EditCurrency = ({ curr, history }) => (curr ? (
  <div className="EditCurrency">
    <h4 className="page-header">{`Editing "${curr.name}"`}</h4>
    <CurrencyEditor curr={curr} history={history} />
  </div>
) : <NotFound />);

EditCurrency.defaultProps = {
  curr: null,
};

EditCurrency.propTypes = {
  curr: PropTypes.object,
  history: PropTypes.object.isRequired,
};

export default createContainer(({ match }) => {
  const currencyId = match.params._id;
  const subscription = Meteor.subscribe('currency.view', currencyId);

  return {
    loading: !subscription.ready(),
    curr: Currency.findOne(currencyId),
  };
}, EditCurrency);
