import React from 'react';
import PropTypes from 'prop-types';
import { ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import Currency from '../../../api/Currency/Currency';
import NotFound from '../NotFound/NotFound';
import Loading from '../../components/Loading/Loading';

const handleRemove = (currencyId, history) => {
  if (confirm('Are you sure? This is permanent!')) {
    Meteor.call('currency.remove', currencyId, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Currency deleted!', 'success');
        history.push('/currencies');
      }
    });
  }
};

const renderCurrency = (curr, match, history) => (curr ? (
  <div className="ViewCurrency">
    <div className="page-header clearfix">
      <h4 className="pull-left">{ curr && curr.name }</h4>
      <ButtonToolbar className="pull-right">
        <ButtonGroup bsSize="small">
          <Button onClick={() => history.push(`${match.url}/edit`)}>Edit</Button>
          <Button onClick={() => handleRemove(curr._id, history)} className="text-danger btn-danger">
            Delete
          </Button>
        </ButtonGroup>
      </ButtonToolbar>
    </div>
    { curr && curr.country }<br />
    { curr && curr.symbol }
  </div>
) : <NotFound />);

const ViewCurrency = ({ loading, curr, match, history }) => (
  !loading ? renderCurrency(curr, match, history) : <Loading />
);

ViewCurrency.propTypes = {
  loading: PropTypes.bool.isRequired,
  curr: PropTypes.object,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default createContainer(({ match }) => {
  const currencyId = match.params._id;
  const subscription = Meteor.subscribe('currency.view', currencyId);

  return {
    loading: !subscription.ready(),
    curr: Currency.findOne(currencyId),
  };
}, ViewCurrency);
