import React from 'react';
import PropTypes from 'prop-types';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import FilesCollection from '../../../api/Files/Files';
import FileEditor from '../../components/FileEditor/FileEditor';
import NotFound from '../NotFound/NotFound';

const EditFile = ({ file, history }) => (file ? (
  <div className="EditFile">
    <h4 className="page-header">{`Editing "${file.meta.name}"`}</h4>
    <FileEditor file={file} history={history} />
  </div>
) : <NotFound />);

EditFile.defaultProps = {
  file: null,
};

EditFile.propTypes = {
  file: PropTypes.object,
  history: PropTypes.object.isRequired,
};

export default createContainer(({ match }) => {
  const fileId = match.params._id;
  const subscription = Meteor.subscribe('file.view', fileId);

  return {
    loading: !subscription.ready(),
    file: FilesCollection.findOne({_id: fileId}),
  };
}, EditFile);
