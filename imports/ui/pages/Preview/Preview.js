import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import 'whatwg-fetch';

import './Preview.scss';

window.addEventListener('resize', (event) => {
  resizeTemplate();
});

const resizeTemplate = () => {
  const width = document.getElementById('Preview').offsetWidth;
  const factor = width/1080;

  const preview = document.getElementById('Preview');
  preview.style.height = `${factor*1920}px`;
  preview.parentElement.style.height = `${factor*1920}px`;
  document.getElementsByClassName('template')[0].style.transform = `scale(${factor})`;
}

const setFooterToBodyNoMargin = () => {
  document.body.style.marginBottom = '0px';
}

const ExchangeItem = ({image, symbol, name, buying, selling}) => (
  <div className="exchangeitem row">
    <div className="col-md-2 col-sm-2 col-xs-2"><div className="exchangeitem-image" style={{backgroundImage: `url(/cdn/storage/${image}.png)`}}></div></div>
    <div className="col-md-4 col-sm-4 col-xs-4 exchangeitem-name">{symbol}</div>
    <div className="col-md-3 col-sm-3 col-xs-3 exchangeitem-buying text-right">{buying > 0 ? parseFloat(buying).toFixed(4) : `-`}</div>
    <div className="col-md-3 col-sm-3 col-xs-3 exchangeitem-selling text-right">{selling > 0 ? parseFloat(selling).toFixed(4) : `-`}</div>
  </div>
);

class Preview extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      exchange: this.props.exchange,
      secondsElapsed: 0,
    }
  }

  tick() {
    this.setState((prevState) => ({
      secondsElapsed: prevState.secondsElapsed + 1
    }));
  }

  componentDidMount() {

    this.interval = setInterval(() => this.tick(), 1000);

    if(this.props.rescale){
      resizeTemplate();
    } else {
      setFooterToBodyNoMargin();
    }

    if(this.props.match){
      console.log(this.props.match.params.date);
      fetch(`/api/exchanges/date/${this.props.match.params.date}`)
        .then((response) => {
          return response.text();
        }).then((body) => {
          const exchanges = JSON.parse(body);
          if(exchanges.Exchanges.length >= 0){
            this.setState({exchange: exchanges.Exchanges[0] });
          }
        });
    }
  }

  componentWillUnmount(){
    clearInterval(this.interval);
  }

  render() {
    return ( <div id="Preview">
      <div className="template">
        {/* <div className="template-content" style={{backgroundImage: `url(/template/forex/forex-poster-portrait.jpg)`}}> */}
        <div className="template-content" style={{}}>
          <video width="1080" height="1920" autoPlay loop style={{position: 'absolute', zIndex: 1}}>
            <source src="/forex-portrait.mp4" type="video/mp4" />
            Your browser does not support the video tag.
          </video>
          <div style={{position: 'absolute', zIndex: 2, width: '1080px'}}>
          <h1 className="header">Foreign Exchange Rate</h1>
          <h2 className="subheader">{moment(this.state.exchange.date).format('dddd D MMM YYYY')} {moment().format('h:mm A')}</h2>
            <div className="exchanges">
              <div className="exchangeitem row">
                <div className="col-md-6 col-sm-6 col-xs-6 exchangeitem-name h">Currency</div>
                <div className="col-md-3 col-sm-3 col-xs-3 exchangeitem-buying h text-center">Buy</div>
                <div className="col-md-3 col-sm-3 col-xs-3 exchangeitem-selling h text-center">Sell</div>
              </div>
              {this.state.exchange && this.state.exchange.exchanges &&
                <div>
                  {this.state.exchange.exchanges.map( exchange => {
                    const ren = exchange.enable === 'on' ? <ExchangeItem key={exchange._id} image={exchange.image} symbol={exchange.symbol} name={exchange.name} buying={exchange.buying} selling={exchange.selling} /> : '';
                    return ren;
                  } )}
                </div>
              }
            {Object.keys(this.state.exchange).length === 0 &&
              <div className="exchangeitem row">
                <div className="col-md-6 col-sm-6 col-xs-4 exchangeitem-name ">-</div>
                <div className="col-md-3 col-sm-3 col-xs-3 exchangeitem-buying  text-center">-</div>
                <div className="col-md-3 col-sm-3 col-xs-3 exchangeitem-selling  text-center">-</div>
              </div>
            }
            </div>
        </div>
          <p className="ps">Rates are subject to change without notice and are valid for transactions up to the equivalent of AUD $100,000</p>
        </div>
      </div>
    </div>)
  }
}

Preview.defaultProps = {
  rescale: false,
  exchange: {},
};

Preview.propTypes = {
  exchange: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default Preview;
