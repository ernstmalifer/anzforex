/* eslint-disable max-len, no-return-assign */

import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup, ControlLabel, Button } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import validate from '../../../modules/validate';

import FileSelection from '../FileSelection/FileSelection';

class CurrencyEditor extends React.Component {
  componentDidMount() {
    const component = this;
    validate(component.form, {
      rules: {
        name: {
          required: true,
        },
        country: {
          required: true,
        },
        symbol: {
          required: true,
        },
        image: {
          required: true,
        }
      },
      messages: {
        name: {
          required: 'Currency name is required.',
        },
        country: {
          required: 'Currency country is required.',
        },
        symbol: {
          required: 'Currency symbol is required.',
        },
        image: {
          required: 'Currency image is required.'
        }
      },
      submitHandler() { component.handleSubmit(); },
    });
  }

  changeFile = (value) => {
    this.image.value = value;
  }

  handleSubmit() {
    const { history } = this.props;
    const existingCurrency = this.props.curr && this.props.curr._id;
    const methodToCall = existingCurrency ? 'currency.update' : 'currency.insert';
    const curr = {
      name: this.name.value.trim(),
      country: this.country.value.trim(),
      symbol: this.symbol.value.trim(),
      image: this.image.value.trim(),
    };

    if (existingCurrency) curr._id = existingCurrency;

    Meteor.call(methodToCall, curr, (error, currencyId) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        const confirmation = existingCurrency ? 'Currency updated!' : 'Currency added!';
        this.form.reset();
        Bert.alert(confirmation, 'success');
        history.push(`/currencies/${currencyId}`);
      }
    });
  }

  render() {
    const { curr } = this.props;
    return (<form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
      <FormGroup>
        <ControlLabel>Currency Name</ControlLabel>
        <input
          type="text"
          className="form-control"
          name="name"
          ref={name => (this.name = name)}
          defaultValue={curr && curr.name}
          placeholder="Currency Name"
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Country</ControlLabel>
        <input
          type="text"
          className="form-control"
          name="country"
          ref={country => (this.country = country)}
          defaultValue={curr && curr.country}
          placeholder="Currency Country"
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Symbol</ControlLabel>
        <input
          type="text"
          className="form-control"
          name="symbol"
          ref={symbol => (this.symbol = symbol)}
          defaultValue={curr && curr.symbol}
          placeholder="Currency Symbol"
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>File</ControlLabel>
        <FileSelection defaultValue={curr && curr.image} onChange={this.changeFile}/>
        <input type="hidden" name="image" defaultValue={curr && curr.image} ref={image => (this.image = image)} />
      </FormGroup>
      <Button type="submit" bsStyle="success">
        {curr && curr._id ? 'Save Changes' : 'Add Currency'}
      </Button>
    </form>);
  }
}

CurrencyEditor.defaultProps = {
  curr: { name: '', country: '', symbol: '', image: '' },
};

CurrencyEditor.propTypes = {
  curr: PropTypes.object,
  history: PropTypes.object.isRequired,
};

export default CurrencyEditor;
