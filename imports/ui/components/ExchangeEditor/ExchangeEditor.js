/* eslint-disable max-len, no-return-assign */

import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup, ControlLabel, Button } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import validate from '../../../modules/validate';
import Loading from '../Loading/Loading';
import CurrencyCollection from '../../../api/Currency/Currency';
import FilesCollection from '../../../api/Files/Files';
import { createContainer } from 'meteor/react-meteor-data';
import { SortableContainer, SortableElement, SortableHandle, arrayMove } from 'react-sortable-hoc';
import XLSX from 'xlsx';
import update from 'immutability-helper';
import moment from 'moment';
import Dropzone from 'react-dropzone';
import UploadsCollection from '../../../api/Uploads/Uploads';
import ExchangesCollection from '../../../api/Exchanges/Exchanges';

import './ExchangeEditor.scss';

function makeProp(prop, value) {
  var obj = {}
  obj[prop] = value
  return obj
}
const DragHandle = SortableHandle(() => <span title="Drag to rearrange" className="drag-handle glyphicon glyphicon-menu-hamburger"></span>);

const SortableItem = SortableElement(({i, currency, files, changeEnable, changeBuying, changeSelling}) => {

  if(currency){
    return(
      <div className="panel panel-default">
        <div className="panel-body">
          <div className="row">
            {/* <div className="col-xs-1 text-center">
              <DragHandle />
            </div> */}
            {/* <div className="col-xs-1 text-center">
              <input type="checkbox" checked={currency.enable === 'on' ? 'checked' : ''} onChange={changeEnable.bind(this,i)}
              disabled="disabled" // Comment if controlled
              />
            </div> */}
            <div className="col-xs-4">
              {files.map((file, index) => {
                if(currency.image === file._id){
                  return (<img key={`file-${file._id}`} className="currency-image" src={`url(${FilesCollection.findOne({_id: file._id}).link()})`} width="30" />);
                } else {
                  return '';
                }
              })}
              <strong>{currency.symbol}</strong> {currency.name}
            </div>
            <div className={`col-xs-4 ${(currency.buying == 0) ? `has-error` : `has-success`}`}>
              <input type="number" className="form-control input-sm" placeholder="Buying" value={currency.buying} onChange={changeBuying.bind(this,i)}
              readOnly // Comment if controlled
              />
            </div>
            <div className={`col-xs-4 ${(currency.buying == 0) ? `has-error` : `has-success`}`}>
              <input type="number" className="form-control input-sm" placeholder="Selling" value={currency.selling} onChange={changeSelling.bind(this,i)} 
              readOnly // Comment if controlled
              />
            </div>
          </div>
        </div>
      </div>
    )
  } else {
    return null;
  }
});

const SortableList = SortableContainer(({currencies, files, changeEnable, changeBuying, changeSelling}) => {
  return (
    <div className="sortable">
      {currencies.map((currency, index) => {
        if(currency.enable === 'on'){
          return (
            <SortableItem key={`item-${index}`} index={index} i={index} currency={currency} files={files} changeEnable={changeEnable} changeBuying={changeBuying} changeSelling={changeSelling} />
          )
        } else {
          return null;
        }
      })}
    </div>
  )
});

class ExchangeEditor extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      debug: false,
      upload: [],
      already: false,
      currencies: this.props.currencies,
      exchangename: this.props.exchange && this.props.exchange.name ? this.props.exchange.name : '',
      // exchangedate: this.props.exchange && this.props.exchange.date ? this.props.exchange.date : moment().format('YYYY-MM-DD'),
      exchangedate: this.props.exchange && this.props.exchange.date ? this.props.exchange.date : '',
    }
  }

  onSortEnd = ({oldIndex, newIndex}) => {
    this.setState({
      currencies: arrayMove(this.state.currencies, oldIndex, newIndex)
    })
  }

  componentDidMount() {
    const component = this;
    validate(component.form, {
      rules: {
        name: {
          required: true,
        },
        date: {
          required: true,
        },
      },
      messages: {
        name: {
          required: 'Exchange name is required.',
        },
        date: {
          required: 'Exchange date is required.',
        },
      },
      submitHandler() { component.handleSubmit(); },
    });
  }

  componentWillReceiveProps = (nextProps) => {
    if(nextProps.currencies && nextProps.exchange && nextProps.exchange._id){
      this.setState({currencies: nextProps.exchange.exchanges})
    } else {
      this.setState({currencies: this.initExchange(nextProps.currencies)});
    }
  }

  changeExchangeName = (event) => {
    this.setState({exchangename: event.target.value});
  }

  changeExchangeDate = (event) => {
    this.setState({exchangedate: event.target.value});
  }

  initExchange = (currencies) => {
    return currencies.map((currency, index) => {
      return Object.assign(currency, {enable: 'off', buying: 0, selling: 0})
    })
  }

  // initEditExchange = (currencies) => {
  //   return currencies.map((currency, index) => {
  //     return Object.assign(currency, {enable: currency.enable, buying: currency.buying, selling: currency.selling})
  //   })
  // }

  changeEnable = (i, event) => {
    // console.log('Enable', i, event.target.checked);
    this.setState(update(this.state, {currencies: makeProp(i, {enable: {$set: event.target.checked ? 'on' : 'off'}})}));
  }

  changeBuying = (i, event) => {
    // console.log('Buying', i, event.target.value);
    this.setState(update(this.state, {currencies: makeProp(i, {buying: {$set: event.target.value}})}));
  }

  changeSelling = (i, event) => {
    // console.log('Selling', i, event.target.value);
    this.setState(update(this.state, {currencies: makeProp(i, {selling: {$set: event.target.value}})}));
  }

  changeFile = (files) => {
    const reader = new FileReader();

    // const file = this.importFile.files[0];
    const file = files[0];
    this.setState({upload: files[0], debug: true});

    reader.onload = (e) => {
        // const data = reader.result;
        let data;
        if (!e) {
          data = reader.content;
        }
        else {
          data = e.target.result;
        }

        try {
          const workbook = XLSX.read(data, {type : 'binary'});

          workbook.SheetNames.forEach((sheetName) => {
              // Here is your object

              const basic = false;
              const XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
              const XL_array = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {header:1});

              if(basic){
                this.parseXLSJSONtoExchanges(XL_row_object);
              } else {
                this.parseXLSJSONtoExchangesANZ(XL_array);
              }
          })
        } catch (e) {
          console.log(e);
          Bert.alert(e, 'warning');
        }
    };

    reader.onerror = (ex) =>{
        Bert.alert(`Error reading file`, 'danger');
    };

    reader.readAsBinaryString(file);

  }

  parseXLSJSONtoExchanges = (json_object) => {
    // console.log(json_object);
    let imported = 0, buyings = 0, sellings = 0;
    json_object.forEach(({Currency, Buying, Selling}) => {
      // Find Index by Currency
      const index = this.state.currencies.findIndex(currency => currency.symbol === Currency);

      if(index !== -1){
        this.setState(update(this.state, {currencies: makeProp(index, {buying: {$set: parseFloat(Buying).toFixed(4)}, selling: {$set: parseFloat(Selling).toFixed(4)}})}));
        if(!isNaN(Buying)){ ++buyings; }
        if(!isNaN(Selling)){ ++sellings; }
        ++imported;
      }

    });

    if(imported > 0){
      Bert.alert(`${imported} exchanges imported. ${buyings} Buyings & ${sellings} Sellings`, 'success');
    } else {
      Bert.alert(`${imported} exchanges imported`, 'default');
    }

  }

  parseXLSJSONtoExchangesANZ = (arr) => {
    // console.log(json_object);
    let imported = 0, buyings = 0, sellings = 0;

    // console.log(arr);
    
    // Date
    // console.log(arr[4][2]);
    // console.log(moment(arr[4][2], 'M/D/YY').format('YYYY-MM-DD'));
    
    // Indexes
    // console.log(arr[7][0], arr[7][0], arr[7][2], arr[7][4]);

    
    [7,8,9,10,11,12,13,14,15,16,17,18].forEach((value, i) => {
      try {
        const index = this.state.currencies.findIndex(currency => currency.symbol === arr[value][0]);
        if(index !== -1){

          let buying = arr[value][2].replace(/,/g, ""), selling = arr[value][4].replace(/,/g, "");

          if(!isNaN(buying)){ ++buyings; } else { buying = 0; }
          if(!isNaN(selling)){ ++sellings; } else { selling = 0; }

          this.setState(update(this.state, {currencies: makeProp(index, {enable: {$set: 'on'}, buying: {$set: parseFloat(buying).toFixed(4)}, selling: {$set: parseFloat(selling).toFixed(4)}})}));
          this.setState({currencies: arrayMove(this.state.currencies, index, i)});
          ++imported;
        }
      } catch(e) {
        console.log(e);
      }
    });
    
    this.setState({exchangename: `Exchange ${moment(arr[4][2], 'M/D/YY').format('YYYY-MM-DD')}`, exchangedate: moment(arr[4][2], 'M/D/YY').format('YYYY-MM-DD')});
    
    this.setState({already: false});
    this.props.exchanges.forEach(exchange => {
      if(exchange.date === moment(arr[4][2], 'M/D/YY').format('YYYY-MM-DD')) {
        console.log('Exchange already exists');
        this.setState({already: true});
      }
    })

    if(imported > 0){
      Bert.alert(`${this.state.already ? `An exchange with date ${moment(arr[4][2], 'M/D/YY').format('YYYY-MM-DD')} exists. ` : ``} ${imported} exchanges imported. ${buyings} Buyings & ${sellings} Sellings`, this.state.already ? 'danger' : 'success');
    } else {
      Bert.alert(`${imported} exchanges imported`, 'default');
    }

  }

  handleSubmit() {
    const { history } = this.props;
    const existingExchange = this.props.exchange && this.props.exchange._id;
    let methodToCall = 'exchange.insert';
    if(existingExchange){
      if(this.props.copy){
        methodToCall = 'exchange.insert'
      } else {
        methodToCall = 'exchange.update'
      }
    } else {
      methodToCall = 'exchange.insert'
    }

    if(this.name.value.trim().length > 0 ){

      const exchange = {
        name: this.name.value.trim(),
        date: this.date.value,
        exchanges: this.state.currencies,
      };
  
      if (existingExchange) exchange._id = existingExchange;

      const fileData = this.state.upload;

      UploadsCollection.insert({
        file: fileData,
        meta: {
          owner: Meteor.userId(),
          createdAt: (new Date()).toISOString(),
          updatedAt: (new Date()).toISOString(),
          name: exchange.name + '.xls'
        },
        onUploaded: function (error, fileObj) {
          if (error) {
            alert('Error during upload: ' + error);
          } else {
            // alert('File "' + fileObj.name + '" successfully uploaded');
            Bert.alert('File successfully uploaded', 'success');
            // history.push(`/files/${fileObj._id}`);
            // history.push(`/files`);

          }
        },
        streams: 'dynamic',
        chunkSize: 'dynamic'
      });

      Meteor.call(methodToCall, exchange, (error, exchangeId) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
          console.log(error);
        } else {
          const confirmation = existingExchange ? 'Exchange updated!' : 'Exchange added!';
          this.form.reset();
          Bert.alert(confirmation, 'success');
          // history.push(`/exchanges/${exchangeId}`);
          history.push(`/exchanges`);
          console.log(exchangeId);
        }
      });


    } else {
      Bert.alert('Please select a file', 'danger');
    }
  }

  render() {
    const { exchange } = this.props;

    return (<form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
      {this.state.already &&
        <div className="alert alert-danger" role="alert">
          An exchange with {this.date.value} date exists.
        </div>
      }
      <FormGroup>
        <ControlLabel>Import Exchange (.xls)</ControlLabel>
        {/* <input
          type="file"
          className=""
          name="importFile"
          accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          ref={importFile => (this.importFile = importFile)}
          onChange={this.changeFile.bind(this)}
          placeholder="File"
        /> */}

        <Dropzone className="droppable" activeClassName="droppable-active" acceptClassName="droppable-accept" rejectClassName="droppable-reject" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ref={importFile => (this.importFile = importFile)} onDrop={this.changeFile.bind(this)} multiple={false}>
          {({ isDragActive, isDragReject, acceptedFiles, rejectedFiles }) => {
            if (isDragActive) {
              return (
                <div>
                  <p><i className="glyphicon glyphicon-save"></i></p>
                  <h2>Select a file here (.xls)</h2>
                  <p>Choose a file or drag it here.</p>
                </div>
              )
            }
            if (isDragReject) {
              return "Please drop only accepted file";
            }
            return acceptedFiles.length || rejectedFiles.length
              ? (
                <div>
                  <p><i className="glyphicon glyphicon-save"></i></p>
                  <h2>{`${acceptedFiles[0].name}`}</h2>
                  <p>&nbsp;</p>
                </div>
              )
              : (
                <div>
                  <p><i className="glyphicon glyphicon-save"></i></p>
                  <h2>Select a file here (.xls)</h2>
                  <p>Choose a file or drag it here.</p>
                </div>
              )
          }}
        </Dropzone>
        
      </FormGroup>


      <div style={{display: this.state.debug ? 'block' : 'none'}}>
        <FormGroup>
          <ControlLabel>Exchange Name</ControlLabel>
          <input
            type="text"
            className="form-control"
            name="name"
            ref={name => (this.name = name)}
            onChange={this.changeExchangeName.bind(this)}
            value={this.state.exchangename}
            placeholder="Exchange Name"
            readOnly // Comment if controlled
          />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Exchange Date</ControlLabel>
          <input
            type="date"
            className="form-control"
            name="date"
            ref={date => (this.date = date)}
            onChange={this.changeExchangeDate.bind(this)}
            value={this.state.exchangedate}
            placeholder="Exchange Date"
            readOnly // Comment if controlled
          />
        </FormGroup>

        <FormGroup>
          <ControlLabel>Exchanges</ControlLabel>
          {!this.props.loading && !this.props.loadingFiles ?
            <SortableList currencies={this.state.currencies} files={this.props.files} onSortEnd={this.onSortEnd} useDragHandle={true} changeEnable={this.changeEnable} changeBuying={this.changeBuying} changeSelling={this.changeSelling}/> 
            :
            <Loading />
          }
        </FormGroup>
      </div>

      <Button type="submit" bsStyle="success">
        {/* exchange && exchange._id ? 'Save Changes' : 'Add Exchange' */}
        {exchange && exchange._id ? 'Save Changes' : 'Upload'}
      </Button>
    </form>);
  }
}

ExchangeEditor.defaultProps = {
  exchange: { name: '' },
};

ExchangeEditor.propTypes = {
  loading: PropTypes.bool.isRequired,
  currencies: PropTypes.arrayOf(PropTypes.object).isRequired,
  history: PropTypes.object.isRequired,
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('currency');
  const filesSubscription = Meteor.subscribe('file');
  const exchangesSubscription = Meteor.subscribe('exchanges');

  return {
    loading: !subscription.ready(),
    currencies: CurrencyCollection.find().fetch(),
    loadingFiles: !filesSubscription.ready(),
    files: FilesCollection.find().fetch(),
    loadingExchanges: !exchangesSubscription.ready(),
    exchanges: ExchangesCollection.find().fetch(),
  };
}, ExchangeEditor);
