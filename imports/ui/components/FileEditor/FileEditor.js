/* eslint-disable max-len, no-return-assign */

import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup, ControlLabel, Button } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import validate from '../../../modules/validate';
import FilesCollection from '../../../api/Files/Files';

class FileEditor extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      fileData: ''
    }
  }

  componentDidMount() {
    const component = this;
    validate(component.form, {
      rules: {
        name: {
          required: true,
        },
        file: {
          required: true,
        },
      },
      messages: {
        name: {
          required: 'File name is required.',
        },
        file: {
          required: 'File is required'
        }
      },
      submitHandler() { component.handleSubmit(); },
    });
  }

  changeFile = (event) => {
    
    const reader = new FileReader();
    const file = this.file.files[0];

    reader.addEventListener("load", ()=> {
      this.setState({fileData: reader.result});
    });

    if(file){
      reader.readAsDataURL(file);
    }

  }

  handleSubmit() {
    const { history } = this.props;
    const existingFile = this.props.file && this.props.file._id;
    const methodToCall = existingFile ? 'file.update' : 'file.insert';

    const fileData = this.file.files[0];

    const file = {
      name: this.name.value.trim(),
    };
    
    if (existingFile) file._id = existingFile;

    if(methodToCall == 'file.insert'){
      FilesCollection.insert({
        file: fileData,
        meta: {
          owner: Meteor.userId(),
          createdAt: (new Date()).toISOString(),
          updatedAt: (new Date()).toISOString(),
          name: file.name
        },
        onUploaded: function (error, fileObj) {
          if (error) {
            alert('Error during upload: ' + error);
          } else {
            // alert('File "' + fileObj.name + '" successfully uploaded');
            Bert.alert('File successfully uploaded', 'success');
            history.push(`/files/${fileObj._id}`);
            // history.push(`/files`);
          }
        },
        streams: 'dynamic',
        chunkSize: 'dynamic'
      });
    } else if(methodToCall === 'file.update'){
      FilesCollection.insert({
        file: fileData,
        meta: {
          owner: Meteor.userId(),
          createdAt: (new Date()).toISOString(),
          updatedAt: (new Date()).toISOString(),
          name: file.name
        },
        onUploaded: function (error, fileObj) {
          if (error) {
            alert('Error during upload: ' + error);
          } else {
            FilesCollection.remove({_id: existingFile}, (error) => {
              if (error) {
                Bert.alert(error.reason, 'danger');
              } else {
                Bert.alert('File successfully uploaded', 'success');
                history.push(`/files/${fileObj._id}`);
              }
            })
          }
        },
        streams: 'dynamic',
        chunkSize: 'dynamic'
      });
    }

  }

  render() {
    const { file } = this.props;
    return (<form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
      <FormGroup>
        <ControlLabel>File Name</ControlLabel>
        <input
          type="text"
          className="form-control"
          name="name"
          ref={name => (this.name = name)}
          defaultValue={file && file.meta && file.meta.name}
          placeholder="File Name"
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>File</ControlLabel>
        {file && file._id &&
          <div className="replace-image" style={{opacity: `${this.state.fileData ? '.2' : '1'}`, marginBottom: '10px'}}>
            <img src={`url(${FilesCollection.findOne({_id: file._id}).link()})`} width="200" />
          </div>
        }
        {this.state.fileData &&
          <div style={{marginBottom: '10px'}}>
            <img src={this.state.fileData} width="200" />
          </div>
        }
        <input
          type="file"
          className=""
          name="file"
          ref={file => (this.file = file)}
          placeholder="File"
          onChange={this.changeFile.bind(this)}
        />
      </FormGroup>
      <Button type="submit" bsStyle="success">
        {file && file._id ? 'Save Changes' : 'Add File'}
      </Button>
    </form>);
  }
}

FileEditor.defaultProps = {
  file: { name: ''},
};

FileEditor.propTypes = {
  file: PropTypes.object,
  history: PropTypes.object.isRequired,
};

export default FileEditor;
