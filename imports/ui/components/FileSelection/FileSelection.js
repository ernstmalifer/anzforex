import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import FilesCollection from '../../../api/Files/Files';
import Loading from '../../components/Loading/Loading';

import './FileSelection.scss';

const handleRemove = (fileId) => {
  // if (confirm('Are you sure? This is permanent!')) {
  //   Meteor.call('file.remove', fileId, (error) => {
  //     if (error) {
  //       Bert.alert(error.reason, 'danger');
  //     } else {
  //       Bert.alert('File deleted!', 'success');
  //     }
  //   });
  // }
  FilesCollection.remove({_id: fileId}, (error) => {
    if (error) {
      Bert.alert(error.reason, 'danger');
    } else {
      Bert.alert('File deleted!', 'success');
      // history.push('/files');
    }
  })
};

// const Files = ({ files, loadingUsers, users, match, history }) => ( !loadingUsers ? (
class Files extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      view: 'grid',
      value: this.props.defaultValue,
    }
  }

  switchView = (view) => {
    this.setState({view});
  }

  setValue = (value) => {
    this.setState({value});
    this.props.onChange(value);
  }

  render() {
    return(
      !this.props.loadingUsers ? (
    <div className="Files">
      <div className="page-header clearfix">
        <div className="pull-right">
          <button className={`btn ${this.state.view === 'list' ? 'btn-primary' : 'btn-default'}`} onClick={this.switchView.bind(this, 'list')} title="List"><i className="glyphicon glyphicon-th-list" /></button> &nbsp;
          <button className={`btn ${this.state.view === 'grid' ? 'btn-primary' : 'btn-default'}`} onClick={this.switchView.bind(this, 'grid')} title="Grid"><i className="glyphicon glyphicon-th" /></button> &nbsp;
          {/* <Link className="btn btn-success" to={`${this.props.match.url}/new`}>Add File</Link> */}
        </div>
      </div>

      {this.props.files.length && this.state.view === 'list' ? 
        <Table responsive>
          <thead>
            <tr>
              <th></th>
              <th>Name</th>
              <th>Last Updated</th>
              <th>Created</th>
            </tr>
          </thead>
          <tbody>
            {this.props.files.map(({ _id, name, type, size, location, meta }) => {
              return (<tr key={_id} className={`row ${this.state.value === _id ? 'active' : ''}`}  onClick={this.setValue.bind(this, _id)}>
                <td><img src={`url(${FilesCollection.findOne({_id: _id}).link()})`} width="50" /></td>
                <td>
                  <h5>{meta.name}</h5>
                  {type}<br />
                  {size}<br />
                </td>
                <td>{timeago(meta.updatedAt)}</td>
                <td>{monthDayYearAtTime(meta.createdAt)}</td>
              </tr>
            )}
            )}
          </tbody>
        </Table> 
        : 
        ''
      }
      {this.props.files.length && this.state.view === 'grid' ? 
      <div className="row">
        {this.props.files.map(({ _id, name, type, size, location, meta }) => {
          return (<div key={_id} className="col-md-2 col-lg-2 col-sm-3 col-xs-4">
            <div className={`thumbnail ${this.state.value === _id ? 'active' : ''}`} style={{minHeight: '150px'}} onClick={this.setValue.bind(this, _id)}>
              <div style={{backgroundImage: `url(${FilesCollection.findOne({_id: _id}).link()})`, backgroundPosition: 'center', backgroundSize: 'contain', backgroundRepeat: 'no-repeat', height: '50px', marginTop: '10px'}} />
              <div style={{padding: '10px'}}>
                <h5>{meta.name}</h5>
                {type}<br />
                {size}
              </div>
            </div>
          </div>)
        })}
      </div>
      :
        ''
      }
      {this.props.files.length === 0 ? 
        <Alert bsStyle="warning">No files yet!</Alert>
        :
        ''
      }
      </div>
    ) : <Loading />)
  }}

Files.propTypes = {
  // loading: PropTypes.bool.isRequired,
  files: PropTypes.arrayOf(PropTypes.object).isRequired,
  defaultValue: PropTypes.string,
  onChange: PropTypes.func
  // history: PropTypes.object.isRequired,
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('file');
  const usersSubscription = Meteor.subscribe('users');

  // console.log(FilesCollection.find().fetch());

  return {
    loading: !subscription.ready(),
    files: FilesCollection.find().fetch(),
    loadingUsers: !usersSubscription.ready(),
    users: Meteor.users.find().fetch()
  };
}, Files);
